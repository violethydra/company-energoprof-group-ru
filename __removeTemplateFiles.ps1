$startLogo = @'
-------------------------------
- Remove template files
-------------------------------
'@

# Params
$MyFolders = 'node_modules', 'public'
$MyFiles = 'yarn.lock', 'package-lock.json', 'yarn-error.log'
$MySingleTmp = 'development/tmp'

# Actions
Remove-Item -Path $MyFolders -Recurse -ErrorAction Ignore 
Remove-Item -Path $MyFiles -Recurse -ErrorAction Ignore 
Remove-Item -Path $MySingleTmp -Recurse -ErrorAction Ignore 

# End
@'
-------------------------------
- DONE!!!
-------------------------------
'@

# Exit to any button
# Write-Host -NoNewLine 'Press any key to exit...';
# $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

$collectName = 'Auto-Close after:';
$collection = '3s', '2s', '1s';

forEach ($item in $collection) {
  Clear-Host;
  Write-Host $startLogo;
  Write-Host $collectName, $item;
  Start-Sleep 1;
}

Exit;

