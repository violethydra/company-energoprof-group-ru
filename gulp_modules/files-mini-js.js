/* ==== IMPORT PARAMS ==== */
'use strict';
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inPub = 'public',
inPubJs = `${inPub}/js`;
/* ==== ----- ==== */


module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	() => combiner(
	
				src(`${inPub}/js/connect.js`, { allowEmpty: true }),
				_run.if(isPublic, _run.babelMinify()),
				_run.rename({ suffix: '.min' }),
				dest(inPubJs)
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));