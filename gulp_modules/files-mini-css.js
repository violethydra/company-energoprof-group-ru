/* ==== IMPORT PARAMS ==== */

/* ==== Sources and directions for files ==== */
const	inPub = 'public';
const	inPubCss = `${inPub}/css`;
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src(`${inPub}/css/global.css`, { allowEmpty: true }),
		_run.if(isPublic, _run.csso()),
		_run.rename({ suffix: '.min' }),
		dest(inPubCss)
	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
