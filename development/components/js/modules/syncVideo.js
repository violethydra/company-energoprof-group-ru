module.exports = class AddOpenMyBurger {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.videoParam = document.querySelector(init.videoBox);
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('MODULE:', this.name, true); }

	main() {
		const syncHeight = () => {
			this.selector.style.height = '';
			if (window.innerWidth >= 575) {
				this.getHeight = this.videoParam.querySelector('video').offsetHeight;
				this.selector.style.height = String(`${this.getHeight}px`);
			}
		};

		window.addEventListener('resize', syncHeight, false);
	}

	run() {
		if (this.selector) {
			this.constructor.info();
			this.main();
		}
	}
};
