module.exports = class AddMoveScrollUP {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.count = Number(Math.abs(init.speed)) || 8;
		this.speed = (this.count <= 20) ? this.count : 8;
		this.myPos = window.pageYOffset;
		this.getScroll = 0; 
		this.speedScroll = this.speed;
	}

	static info() { console.log('MODULE:', this.name, true); }

	scrollUpSetting() {
		const clickedArrow = () => {
			this.getScroll = document.documentElement.scrollTop; 
			if (this.getScroll >= 1) {
				window.requestAnimationFrame(clickedArrow);
				window.scrollTo(0, this.getScroll - this.getScroll / this.speedScroll);
			}
		};
		const scrolledDown = () => {
			this.myPos = window.pageYOffset;
			(this.myPos >= 100) ? this.selector.classList.add('show') : this.selector.classList.remove('show');
		};

		this.selector.addEventListener('click', clickedArrow, false);
		window.addEventListener('scroll', scrolledDown, false);
	}

	run() {
		if (this.selector) {
			this.constructor.info();
			this.scrollUpSetting();
		}
	}
};
