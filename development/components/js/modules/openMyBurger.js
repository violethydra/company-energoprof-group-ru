module.exports = class AddOpenMyBurger {
	constructor(init) {
		this.selector = document.querySelector(init.burger);
		this.navbar = document.querySelector(init.navbar);
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('MODULE:', this.name, true); }

	toggleBurger() {
		const openBurger = () => {
			const elem = this.navbar.querySelector('DIV');
			const count = elem.childElementCount;
			const countString = `${count * 2 + count / 2}rem`;
			
			elem.classList.toggle('open');
			elem.style.paddingBottom = '';
			
			if (elem.classList.contains('open')) elem.style.paddingBottom = countString;
			
			this.selector.classList.toggle('open');
		};
		this.selector.addEventListener('click', openBurger, false);
	}

	run() {
		if (this.selector) {
			this.constructor.info();
			this.toggleBurger();
		}
	}
};
