// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddOpenMyBurger from './modules/openMyBurger';
import AddsyncVideo from './modules/syncVideo';

const start = (x) => {
	if (x === true) {
		console.log('DOM:', 'DOMContentLoaded', x);

		new AddsyncVideo({
			selector: '.js__syncVideo',
			videoBox: '.js__syncVideoBox'
		}).run();

		new AddMoveScrollUP({
			selector: '.js__moveScrollUP',
			speed: 8
		}).run();

		new AddOpenMyBurger({
			burger: '.js__navHamburger',
			navbar: '.js__navHamburgerOpener'
		}).run();

		const showsecret = () => {
			const qs = document.querySelector('#myform-password');
			const list = document.querySelector('.js__showsecret');
			
			const func = () => {
				if (qs.getAttribute('type') === 'password') {
					qs.setAttribute('type', 'text');
					list.style.fill = '#007cad';
				} else {
					qs.setAttribute('type', 'password');
					list.style.fill = '';
				}
			};

			list.addEventListener('click', func, false);
		};
		showsecret();

	} else { console.log('System: ', 'I think shit happens 😥 '); }

};

const addCss = (fileName) => {
	const link = document.createElement('link');
	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = fileName;

	document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {

	if (navigator.userAgent.includes('Firefox')) addCss('css/firefox.css');
	if (navigator.userAgent.includes('Edge')) addCss('css/edge.css');

	document.addEventListener('DOMContentLoaded', start(true), false);
}
