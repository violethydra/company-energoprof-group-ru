/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./development/components/js/connect.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/openMyBurger */ "./development/components/js/modules/openMyBurger.js");
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _modules_syncVideo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/syncVideo */ "./development/components/js/modules/syncVideo.js");
/* harmony import */ var _modules_syncVideo__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_modules_syncVideo__WEBPACK_IMPORTED_MODULE_2__);
// ============================
//    Name: index.js
// ============================




var start = function start(x) {
  if (x === true) {
    console.log('DOM:', 'DOMContentLoaded', x);
    new _modules_syncVideo__WEBPACK_IMPORTED_MODULE_2___default.a({
      selector: '.js__syncVideo',
      videoBox: '.js__syncVideoBox'
    }).run();
    new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default.a({
      selector: '.js__moveScrollUP',
      speed: 8
    }).run();
    new _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1___default.a({
      burger: '.js__navHamburger',
      navbar: '.js__navHamburgerOpener'
    }).run();

    var showsecret = function showsecret() {
      var qs = document.querySelector('#myform-password');
      var list = document.querySelector('.js__showsecret');

      var func = function func() {
        if (qs.getAttribute('type') === 'password') {
          qs.setAttribute('type', 'text');
          list.style.fill = '#007cad';
        } else {
          qs.setAttribute('type', 'password');
          list.style.fill = '';
        }
      };

      list.addEventListener('click', func, false);
    };

    showsecret();
  } else {
    console.log('System: ', 'I think shit happens 😥 ');
  }
};

var addCss = function addCss(fileName) {
  var link = document.createElement('link');
  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = fileName;
  document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  if (navigator.userAgent.includes('Firefox')) addCss('css/firefox.css');
  if (navigator.userAgent.includes('Edge')) addCss('css/edge.css');
  document.addEventListener('DOMContentLoaded', start(true), false);
}

/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddMoveScrollUP(init) {
    _classCallCheck(this, AddMoveScrollUP);

    this.selector = document.querySelector(init.selector);
    this.count = Number(Math.abs(init.speed)) || 8;
    this.speed = this.count <= 20 ? this.count : 8;
    this.myPos = window.pageYOffset;
    this.getScroll = 0;
    this.speedScroll = this.speed;
  }

  _createClass(AddMoveScrollUP, [{
    key: "scrollUpSetting",
    value: function scrollUpSetting() {
      var _this = this;

      var clickedArrow = function clickedArrow() {
        _this.getScroll = document.documentElement.scrollTop;

        if (_this.getScroll >= 1) {
          window.requestAnimationFrame(clickedArrow);
          window.scrollTo(0, _this.getScroll - _this.getScroll / _this.speedScroll);
        }
      };

      var scrolledDown = function scrolledDown() {
        _this.myPos = window.pageYOffset;
        _this.myPos >= 100 ? _this.selector.classList.add('show') : _this.selector.classList.remove('show');
      };

      this.selector.addEventListener('click', clickedArrow, false);
      window.addEventListener('scroll', scrolledDown, false);
    }
  }, {
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.scrollUpSetting();
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddMoveScrollUP;
}();

/***/ }),

/***/ "./development/components/js/modules/openMyBurger.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/openMyBurger.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddOpenMyBurger(init) {
    _classCallCheck(this, AddOpenMyBurger);

    this.selector = document.querySelector(init.burger);
    this.navbar = document.querySelector(init.navbar);
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddOpenMyBurger, [{
    key: "toggleBurger",
    value: function toggleBurger() {
      var _this = this;

      var openBurger = function openBurger() {
        var elem = _this.navbar.querySelector('DIV');

        var count = elem.childElementCount;
        var countString = "".concat(count * 2 + count / 2, "rem");
        elem.classList.toggle('open');
        elem.style.paddingBottom = '';
        if (elem.classList.contains('open')) elem.style.paddingBottom = countString;

        _this.selector.classList.toggle('open');
      };

      this.selector.addEventListener('click', openBurger, false);
    }
  }, {
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.toggleBurger();
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddOpenMyBurger;
}();

/***/ }),

/***/ "./development/components/js/modules/syncVideo.js":
/*!********************************************************!*\
  !*** ./development/components/js/modules/syncVideo.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddOpenMyBurger(init) {
    _classCallCheck(this, AddOpenMyBurger);

    this.selector = document.querySelector(init.selector);
    this.videoParam = document.querySelector(init.videoBox);
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddOpenMyBurger, [{
    key: "main",
    value: function main() {
      var _this = this;

      var syncHeight = function syncHeight() {
        _this.selector.style.height = '';

        if (window.innerWidth >= 575) {
          _this.getHeight = _this.videoParam.querySelector('video').offsetHeight;
          _this.selector.style.height = String("".concat(_this.getHeight, "px"));
        }
      };

      window.addEventListener('resize', syncHeight, false);
    }
  }, {
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.main();
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddOpenMyBurger;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL21vdmVTY3JvbGxVUC5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvb3Blbk15QnVyZ2VyLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9zeW5jVmlkZW8uanMiXSwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanNcIik7XG4iLCIvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbi8vICAgIE5hbWU6IGluZGV4LmpzXHJcbi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuXHJcbmltcG9ydCBBZGRNb3ZlU2Nyb2xsVVAgZnJvbSAnLi9tb2R1bGVzL21vdmVTY3JvbGxVUCc7XHJcbmltcG9ydCBBZGRPcGVuTXlCdXJnZXIgZnJvbSAnLi9tb2R1bGVzL29wZW5NeUJ1cmdlcic7XHJcbmltcG9ydCBBZGRzeW5jVmlkZW8gZnJvbSAnLi9tb2R1bGVzL3N5bmNWaWRlbyc7XHJcblxyXG5jb25zdCBzdGFydCA9ICh4KSA9PiB7XHJcblx0aWYgKHggPT09IHRydWUpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdET006JywgJ0RPTUNvbnRlbnRMb2FkZWQnLCB4KTtcclxuXHJcblx0XHRuZXcgQWRkc3luY1ZpZGVvKHtcclxuXHRcdFx0c2VsZWN0b3I6ICcuanNfX3N5bmNWaWRlbycsXHJcblx0XHRcdHZpZGVvQm94OiAnLmpzX19zeW5jVmlkZW9Cb3gnXHJcblx0XHR9KS5ydW4oKTtcclxuXHJcblx0XHRuZXcgQWRkTW92ZVNjcm9sbFVQKHtcclxuXHRcdFx0c2VsZWN0b3I6ICcuanNfX21vdmVTY3JvbGxVUCcsXHJcblx0XHRcdHNwZWVkOiA4XHJcblx0XHR9KS5ydW4oKTtcclxuXHJcblx0XHRuZXcgQWRkT3Blbk15QnVyZ2VyKHtcclxuXHRcdFx0YnVyZ2VyOiAnLmpzX19uYXZIYW1idXJnZXInLFxyXG5cdFx0XHRuYXZiYXI6ICcuanNfX25hdkhhbWJ1cmdlck9wZW5lcidcclxuXHRcdH0pLnJ1bigpO1xyXG5cclxuXHRcdGNvbnN0IHNob3dzZWNyZXQgPSAoKSA9PiB7XHJcblx0XHRcdGNvbnN0IHFzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI215Zm9ybS1wYXNzd29yZCcpO1xyXG5cdFx0XHRjb25zdCBsaXN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmpzX19zaG93c2VjcmV0Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBmdW5jID0gKCkgPT4ge1xyXG5cdFx0XHRcdGlmIChxcy5nZXRBdHRyaWJ1dGUoJ3R5cGUnKSA9PT0gJ3Bhc3N3b3JkJykge1xyXG5cdFx0XHRcdFx0cXMuc2V0QXR0cmlidXRlKCd0eXBlJywgJ3RleHQnKTtcclxuXHRcdFx0XHRcdGxpc3Quc3R5bGUuZmlsbCA9ICcjMDA3Y2FkJztcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0cXMuc2V0QXR0cmlidXRlKCd0eXBlJywgJ3Bhc3N3b3JkJyk7XHJcblx0XHRcdFx0XHRsaXN0LnN0eWxlLmZpbGwgPSAnJztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRsaXN0LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuYywgZmFsc2UpO1xyXG5cdFx0fTtcclxuXHRcdHNob3dzZWNyZXQoKTtcclxuXHJcblx0fSBlbHNlIHsgY29uc29sZS5sb2coJ1N5c3RlbTogJywgJ0kgdGhpbmsgc2hpdCBoYXBwZW5zIPCfmKUgJyk7IH1cclxuXHJcbn07XHJcblxyXG5jb25zdCBhZGRDc3MgPSAoZmlsZU5hbWUpID0+IHtcclxuXHRjb25zdCBsaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGluaycpO1xyXG5cdGxpbmsudHlwZSA9ICd0ZXh0L2Nzcyc7XHJcblx0bGluay5yZWwgPSAnc3R5bGVzaGVldCc7XHJcblx0bGluay5ocmVmID0gZmlsZU5hbWU7XHJcblxyXG5cdGRvY3VtZW50LmhlYWQuYXBwZW5kQ2hpbGQobGluayk7XHJcbn07XHJcblxyXG5pZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93ICYmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKSB7XHJcblxyXG5cdGlmIChuYXZpZ2F0b3IudXNlckFnZW50LmluY2x1ZGVzKCdGaXJlZm94JykpIGFkZENzcygnY3NzL2ZpcmVmb3guY3NzJyk7XHJcblx0aWYgKG5hdmlnYXRvci51c2VyQWdlbnQuaW5jbHVkZXMoJ0VkZ2UnKSkgYWRkQ3NzKCdjc3MvZWRnZS5jc3MnKTtcclxuXHJcblx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIHN0YXJ0KHRydWUpLCBmYWxzZSk7XHJcbn1cclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRNb3ZlU2Nyb2xsVVAge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQuc2VsZWN0b3IpO1xyXG5cdFx0dGhpcy5jb3VudCA9IE51bWJlcihNYXRoLmFicyhpbml0LnNwZWVkKSkgfHwgODtcclxuXHRcdHRoaXMuc3BlZWQgPSAodGhpcy5jb3VudCA8PSAyMCkgPyB0aGlzLmNvdW50IDogODtcclxuXHRcdHRoaXMubXlQb3MgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblx0XHR0aGlzLmdldFNjcm9sbCA9IDA7IFxyXG5cdFx0dGhpcy5zcGVlZFNjcm9sbCA9IHRoaXMuc3BlZWQ7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdHNjcm9sbFVwU2V0dGluZygpIHtcclxuXHRcdGNvbnN0IGNsaWNrZWRBcnJvdyA9ICgpID0+IHtcclxuXHRcdFx0dGhpcy5nZXRTY3JvbGwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wOyBcclxuXHRcdFx0aWYgKHRoaXMuZ2V0U2Nyb2xsID49IDEpIHtcclxuXHRcdFx0XHR3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGNsaWNrZWRBcnJvdyk7XHJcblx0XHRcdFx0d2luZG93LnNjcm9sbFRvKDAsIHRoaXMuZ2V0U2Nyb2xsIC0gdGhpcy5nZXRTY3JvbGwgLyB0aGlzLnNwZWVkU2Nyb2xsKTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcdGNvbnN0IHNjcm9sbGVkRG93biA9ICgpID0+IHtcclxuXHRcdFx0dGhpcy5teVBvcyA9IHdpbmRvdy5wYWdlWU9mZnNldDtcclxuXHRcdFx0KHRoaXMubXlQb3MgPj0gMTAwKSA/IHRoaXMuc2VsZWN0b3IuY2xhc3NMaXN0LmFkZCgnc2hvdycpIDogdGhpcy5zZWxlY3Rvci5jbGFzc0xpc3QucmVtb3ZlKCdzaG93Jyk7XHJcblx0XHR9O1xyXG5cclxuXHRcdHRoaXMuc2VsZWN0b3IuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBjbGlja2VkQXJyb3csIGZhbHNlKTtcclxuXHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBzY3JvbGxlZERvd24sIGZhbHNlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGlmICh0aGlzLnNlbGVjdG9yKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0XHR0aGlzLnNjcm9sbFVwU2V0dGluZygpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRPcGVuTXlCdXJnZXIge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQuYnVyZ2VyKTtcclxuXHRcdHRoaXMubmF2YmFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcihpbml0Lm5hdmJhcik7XHJcblx0XHR0aGlzLmVycm9yVGV4dCA9ICfwn5KAIE91Y2gsIGRhdGFiYXNlIGVycm9yLi4uJztcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkgeyBjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHJcblx0dG9nZ2xlQnVyZ2VyKCkge1xyXG5cdFx0Y29uc3Qgb3BlbkJ1cmdlciA9ICgpID0+IHtcclxuXHRcdFx0Y29uc3QgZWxlbSA9IHRoaXMubmF2YmFyLnF1ZXJ5U2VsZWN0b3IoJ0RJVicpO1xyXG5cdFx0XHRjb25zdCBjb3VudCA9IGVsZW0uY2hpbGRFbGVtZW50Q291bnQ7XHJcblx0XHRcdGNvbnN0IGNvdW50U3RyaW5nID0gYCR7Y291bnQgKiAyICsgY291bnQgLyAyfXJlbWA7XHJcblx0XHRcdFxyXG5cdFx0XHRlbGVtLmNsYXNzTGlzdC50b2dnbGUoJ29wZW4nKTtcclxuXHRcdFx0ZWxlbS5zdHlsZS5wYWRkaW5nQm90dG9tID0gJyc7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoZWxlbS5jbGFzc0xpc3QuY29udGFpbnMoJ29wZW4nKSkgZWxlbS5zdHlsZS5wYWRkaW5nQm90dG9tID0gY291bnRTdHJpbmc7XHJcblx0XHRcdFxyXG5cdFx0XHR0aGlzLnNlbGVjdG9yLmNsYXNzTGlzdC50b2dnbGUoJ29wZW4nKTtcclxuXHRcdH07XHJcblx0XHR0aGlzLnNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgb3BlbkJ1cmdlciwgZmFsc2UpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0aWYgKHRoaXMuc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdHRoaXMudG9nZ2xlQnVyZ2VyKCk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IGNsYXNzIEFkZE9wZW5NeUJ1cmdlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5zZWxlY3Rvcik7XHJcblx0XHR0aGlzLnZpZGVvUGFyYW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQudmlkZW9Cb3gpO1xyXG5cdFx0dGhpcy5lcnJvclRleHQgPSAn8J+SgCBPdWNoLCBkYXRhYmFzZSBlcnJvci4uLic7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdG1haW4oKSB7XHJcblx0XHRjb25zdCBzeW5jSGVpZ2h0ID0gKCkgPT4ge1xyXG5cdFx0XHR0aGlzLnNlbGVjdG9yLnN0eWxlLmhlaWdodCA9ICcnO1xyXG5cdFx0XHRpZiAod2luZG93LmlubmVyV2lkdGggPj0gNTc1KSB7XHJcblx0XHRcdFx0dGhpcy5nZXRIZWlnaHQgPSB0aGlzLnZpZGVvUGFyYW0ucXVlcnlTZWxlY3RvcigndmlkZW8nKS5vZmZzZXRIZWlnaHQ7XHJcblx0XHRcdFx0dGhpcy5zZWxlY3Rvci5zdHlsZS5oZWlnaHQgPSBTdHJpbmcoYCR7dGhpcy5nZXRIZWlnaHR9cHhgKTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgc3luY0hlaWdodCwgZmFsc2UpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0aWYgKHRoaXMuc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdHRoaXMubWFpbigpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTNCQTtBQUFBO0FBQUE7QUE4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxDQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBVkE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBdkJBO0FBQUE7QUFBQTtBQTBCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBOUJBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFQQTtBQUNBO0FBREE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuQkE7QUFBQTtBQUFBO0FBc0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExQkE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQVBBO0FBQ0E7QUFEQTtBQUFBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=